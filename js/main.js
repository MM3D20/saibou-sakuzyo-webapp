/**
 * SAIBOU_SAKUZYO
 * A script modified from the game of life in order to similate the model
 * @author Haochen Zheng
 * @copyright  2018 Haochen Zheng https://sparta-en.org/
 * @version 0.0.1
 * @license CC-BY-NC-SA-4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
 */
var g_x, g_y;
var clr_m = 10;
var cgraph;
var metric;
var cnt = 0;
var debug = false;
var step = 0;
var onview = false;
var quickdraw = false;
$(document).ready(function () {
    g_x = $('input[name=row-width]')[0].value;
    g_y = $('input[name=row-height]')[0].value;
    metric = $('input[name=metric]')[0].value;
    // Construct the graph automacally
    $('input[name=row-width]').on('input', function (e) {
        var tmp;
        tmp = $('input[name=row-width]')[0].value;
        if (isNaN(tmp)) {
            $('input[name=row-width]')[0].value = g_x;
        } else {
            g_x = tmp;
            section_update();
            log("Graph Updated!");
        }
    });
    $('input[name=row-height]').on('input', function (e) {
        var tmp;
        tmp = $('input[name=row-height]')[0].value;
        if (isNaN(tmp)) {
            $('input[name=row-height]')[0].value = g_y;
        } else {
            g_y = tmp;
            section_update();
            log("Graph Updated!");
        }
    });
    $('input[name=mheat]').on('input', function (e) {
        var tmp;
        tmp = $('input[name=mheat]')[0].value;
        if (isNaN(tmp)) {
            $('input[name=mheat]')[0].value = clr_m;
        } else {
            clr_m = tmp;
            timemachine(0);
            log("Heat Mapping Updated");
        }
    });
    $('input[name=metric]').on('input', function (e) {
        var tmp;
        tmp = $('input[name=metric]')[0].value;
        if (isNaN(tmp)) {
            $('input[name=metric]')[0].value = metric;
        } else {
            metric = tmp;
        }
    });
    section_update();
    log("Press 'q' to Enable Quickdraw");
})
// Lieten for hotkey for quickdraw(Default q:81)
$(document).bind('keyup', function (e) {
    if (e.keyCode == 81) {
        if (quickdraw == true) {
            quickdraw = false;
            log("Quickdraw Disabled!");
        } else {
            quickdraw = true;
            log("Quickdraw Enabled!")
        }
    }
});
// The function to construct the graph
function section_update() {
    cgraph = [];
    for (var i = 0; i < Number(g_x) + 2; i++) {
        cgraph[i] = [];
        for (var j = 0; j < Number(g_y) + 2; j++) {
            cgraph[i][j] = [];
            cgraph[i][j][0] = 0;
        }
    }
    $('#graph').empty();
    for (var i = 0; i < Number(g_x) + 2; i++) {
        var tmp = "<div " + "id='row" + i + "'>"
        $('#graph').append(tmp);
        for (var j = 0; j < Number(g_y) + 2; j++) {
            var proc = '#pixel' + i + '_' + j;
            var tmp = "<div id='pixel" + i + "_" + j + "'></div>";
            var row = "#row" + i;
            $(row).append(tmp);
            if (i != 0 && j != 0 && i != Number(g_x) + 1 && j != Number(g_y) + 1) {
                $(proc).on('click', function (e) {
                    // Parse user input
                    var clicked = $(this).attr('id');
                    var id = clicked.match(/^pixel(\d+)_\s*(\d+)$/);
                    if (onview == false) {
                        cgraph[id[1]][id[2]][0] = metric;
                        $(this).css('background-color', gen_color(metric));
                    }
                });
                $(proc).on('mouseenter', function (e) {
                    // Parse user input
                    if (quickdraw == true) {
                        var clicked = $(this).attr('id');
                        var id = clicked.match(/^pixel(\d+)_\s*(\d+)$/);
                        if (onview == false) {
                            cgraph[id[1]][id[2]][0] = metric;
                            $(this).css('background-color', gen_color(metric));
                        }
                    }
                });
            }
        }
    }
}
// Graph changes modifier
function section_render(x, y, met) {
    var pos = "#pixel" + x + "_" + y;
    $(pos).css("background-color", gen_color(met));
}
// Log management (append)
function log(str) {
    $("#log_section").append(str + "<br />");
    console.info(str);
}
// Log management (empty)
function clear_log() {
    $("#log_section").empty();
}
// Get the color of the unit
function gen_color(met) {
    var deg = Number(met) / Number(clr_m);
    if (0 <= deg && deg < 0.25) {
        return "#2C95DD";
    } else if (0.25 <= deg && deg < 0.5) {
        var sec = deg * 4 - 1;
        var r = Math.floor(44 + 202 * sec);
        var g = Math.floor(149 + 67 * sec);
        var b = Math.floor(221 - 119 * sec);
        return rgb2hex(r, g, b);
    } else if (0.5 <= deg && deg < 0.75) {
        var sec = deg * 4 - 2;
        var r = Math.floor(246 + 9 * sec);
        var g = Math.floor(126 - 89 * sec);
        var b = Math.floor(102 - 48 * sec);
        return rgb2hex(r, g, b);
    } else if (0.75 <= deg && deg < 1) {
        var sec = deg * 4 - 3;
        var r = Math.floor(255 - 3 * sec);
        var g = Math.floor(127 - 72 * sec);
        var b = Math.floor(54 + 68 * sec);
        return rgb2hex(r, g, b);
    } else {
        return "#FC3770";
    }
}
// Convert RGB to HEX
function rgb2hex(r, g, b) {
    return "#" + comhex(r) + comhex(g) + comhex(b);
}
// Sub function to convert number to hex
function comhex(n) {
    var hex = n.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}
// Space initial
function init(cnt) {
    for (var i = 0; i < Number(g_x) + 2; i++) {
        for (var j = 0; j < Number(g_y) + 2; j++) {
            cgraph[i][j][cnt] = 0;
        }
    }
}
// Sleep function
function sleep(delay) {
    var start = new Date().getTime();
    while (new Date().getTime() < start + delay);
}
// Rerender the graph at the specific time
function timemachine(time) {
    for (var i = 0; i < Number(g_x) + 2; i++) {
        for (var j = 0; j < Number(g_y) + 2; j++) {
            section_render(i, j, cgraph[i][j][time]);
        }
    }
}
// Print status in log section
function output_graph() {
    if (debug == true) {
        var str = 'Count : ' + cnt;
        log(str);
        for (var i = 0; i < Number(g_x) + 2; i++) {
            var str = '';
            for (var j = 0; j < Number(g_y) + 2; j++) {
                str = str + cgraph[i][j][cnt] + " ";
            }
            log(str);
        }
    }
}
// END OF BASIC FRAMEWORK
// The MAIN function to start graphing
function graph() {
    $('#status-bar').css('background-color', '#ff9d2d');
    $('#option-bar').css('display', 'none');
    $('#view-bar').css('display', 'block');
    log('Started');
    cnt = 0;
    while (is_over(cnt) === false) {
        init(cnt + 1);
        for (var i = 1; i < Number(g_x) + 1; i++) {
            for (var j = 1; j < Number(g_y) + 1; j++) {
                var cost = settlement(i, j, cnt);
                if ((cgraph[i][j][cnt] - cost) < 0) {
                    cgraph[i][j][cnt + 1] = 0
                } else {
                    cgraph[i][j][cnt + 1] = cgraph[i][j][cnt] - cost;
                }
            }
        }
        output_graph();
        cnt++;
    }
    output_graph();
    log("Completed with " + cnt + " times!");
    onview = true;
}
// Check if the progress is over 
function is_over(cnt) {
    var tot = 0;
    for (var i = 0; i < Number(g_x) + 2; i++) {
        for (var j = 0; j < Number(g_y) + 2; j++) {
            tot = tot + cgraph[i][j][cnt];
        }
    }
    if (tot != 0) {
        return false;
    } else {
        return true;
    }
}
// Get the number should be minored
function settlement(x, y, cnt) {
    var cost = 0;
    if (cgraph[x][y][cnt] == 0) {
        return 0;
    }
    for (var i = x - 1; i <= x + 1; i++) {
        for (var j = y - 1; j <= y + 1; j++) {
            if (i != x || j != y) {
                if (cgraph[i][j][cnt] == 0) {
                    cost++;
                }
            }
        }
    }
    return cost;
}
// VIEW Step forward
function setp_forward() {
    step++;
    if (step > cnt) {
        log("Maxium Steps Exceed!");
        step--;
    } else {
        timemachine(step);
    }
}
// VIEW Step back
function step_back() {
    step--;
    if (step < 0) {
        log("Minimum Step Exceed!")
        step++;
    } else {
        timemachine(step);
    }
}
// Output current value
function output_value() {
    var str = 'Count : ' + step;
    log(str);
    for (var i = 0; i < Number(g_x) + 2; i++) {
        var str = '';
        for (var j = 0; j < Number(g_y) + 2; j++) {
            str = str + cgraph[i][j][step] + " ";
        }
        log(str);
    }
}
// Exit view mode
function exit_view() {
    $('#status-bar').css('background-color', '#2db5ff');
    step = 0;
    $('#option-bar').css('display', 'block');
    $('#view-bar').css('display', 'none');
    onview = false;
    timemachine(0);
}
